import React, { Component } from "react";
import FloatingActionButton from "material-ui/FloatingActionButton";
import IconButton from "material-ui/IconButton";
import FontIcon from "material-ui/FontIcon";
import ContentAdd from "material-ui/svg-icons/content/add";

class App extends Component {
  state = {
    open: false
  };

  handleToggle = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    return (
      <div style={{ position: "fixed", bottom: "2em", right: "2em" }}>
        <FloatingActionButton onClick={this.handleToggle}>
          <ContentAdd />
        </FloatingActionButton>
        {this.state.open && (
          <div
            style={{
              position: "fixed",
              background: "#FFFFFF",
              color: "#B3B3B3",
              height: "80vh",
              width: "23vw",
              bottom: "7em",
              right: "2em",
              boxShadow:
                "0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)",
              borderRadius: "2em"
            }}
          >
            <div style={{ height: "70%" }}>
              <div
                style={{
                  height: "80%",
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  textAlign: "center"
                }}
              >
                <img src="https://picsum.photos/200/200?image=0" />
              </div>
              <div style={{ height: "20%", textAlign: "center" }}>
                Hello<br />i am (BIZ) bot. How can I<br />help you today?
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-around",
                height: "27%"
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  height: "90%",
                  marginBottom: "2em"
                }}
              >
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    borderRadius: "0.5em",
                    alignItems: "center",
                    border: "0.5px solid #B3B3B3",
                    width: "90%",
                    background: "white"
                  }}
                >
                  <div style={{ padding: "0 2em", color: "#3C7FC8" }}>
                    <i className="material-icons">forum</i>
                  </div>
                  <div>Talk to our experts</div>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  height: "90%",
                  marginBottom: "2em"
                }}
              >
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    border: "0.5px solid #B3B3B3",
                    borderRadius: "0.5em",
                    width: "90%",
                    background: "white"
                  }}
                >
                  <div style={{ padding: "0 2em", color: "#3C7FC8" }}>
                    <i className="material-icons">local_play</i>
                  </div>
                  <div>Post your Inquiry/Issues</div>
                </div>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center"
                }}
              >
                Powered by Prackr
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default App;
